#!/usr/bin/env bash

# Install AWS CLI
pip install awsebcli --upgrade --ignore-installed

# Prepare deployment
mkdir ~/.aws/
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
touch ~/.aws/config
aws configure set default.region us-east-2

#printf "[profile default]\nregion=us-east-2\noutput=json" >> ~/.aws/config
#printf "[profile eb-cli]\nregion=us-east-2\noutput=json" >> ~/.aws/config

aws elasticbeanstalk create-application --application-name docker-nodejs-app --description "my application"
aws elasticbeanstalk create-environment --application-name docker-nodejs-app --environment-name docker-nodejs-env --cname-prefix docker-nodejs-app --solution-stack-name "64bit Amazon Linux 2 v3.0.0 running Docker" --option-settings "Namespace=aws:autoscaling:launchconfiguration,OptionName=IamInstanceProfile,Value=aws-elasticbeanstalk-ec2-role"

#yes n | eb init --profile eb-cli
eb list
